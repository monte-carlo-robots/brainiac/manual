# Construction Overview
Brainiac construction instructions.

The objective of the Brainiac construction instructions is to provide anyone the necessary material list and steps to construct the Brainiac as we made.

The Brainiac was designed to navigate at the floor of indoor environments. Also, it was designed to identify if the floor is black or white. One goal in its design was to achieve the most possible precision either moving or sensing the floor color. This decision was made to decrease the variance in Monte Carlo Localization helping it to self localize the robot.

Thus, you don't need to strictly follow these construction instructions to run your robot. But be aware that making it different would lead to different mathematical modeling parameters and maybe different hardware drivers.

The construction instructions are divided into 3 parts:

- [Required Parts](./required_parts.md): The hardware parts used in the Brainiac necessary to construct it.
- [Assembly](./assembly.md): The assembly instruction using the required parts. In the end, the Brainiac construction should be completely ready.
- [Testing](./testing.md): The hardware testing, to verify if the motors and sensors are correctly connected.
