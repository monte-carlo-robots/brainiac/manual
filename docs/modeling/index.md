## Modeling Overview

Brainiac mathematical modeling. 

The objective of the Brainiac mathematical modeling is to get the dynamic 
equations and probabilities distribution that models the Brainiac. 

In order to run the Monte Carlo Localization algorithm and also the Kalman Filter
we need to know all these models. The models give us information about the 
expected behaviors and how confident it is. For example, to a robot above a black
ground, the model tells us that most measurements should tell that the robot is above
the black ground and how many wrong readings we should expect.

The mathematical modeling and its experiments to parametrize it is almost an art.
Approximations or simplifications may lead to more simple and robust modeling
but is less precise than a more complex one.

As this project is being developed in the quarantine context of COVID-19 disease,
we don't have access to the labs. Thus, we will do the most precise we can, but 
using many datasheets information that we could verify experimentally.

We divided the modeling section per physical component. At each one we cover 
some theory of the mathematical models we are assuming and how we parametrized
it. We expect some background to understand these modeling, but we provide
references to base explanations.

The physical components models are divided as:

- [Motor](./modeling/motor.md) 
- [Motor Encoder](./modeling/motor_encoder.md)
- [IR Sensor](./modeling/ir_sensor.md)
- [IMU](./modeling/imu.md)
- [Movement](./modeling/movement.md)
