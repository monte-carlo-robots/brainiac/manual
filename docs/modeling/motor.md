## Motor mathematical modeling

The Brainiac DC Motors mathematical modeling.

The DC motor model is a well-known model studied by roboticists from all over the
world. If you never saw it, we strongly recommend you to study it first at [this
tutorial](http://ctms.engin.umich.edu/CTMS/index.php?example=MotorSpeed&section=SystemModeling).

Special thanks to my friend [Igor Novais](http://lattes.cnpq.br/0207427378059224)
that helped us with modeling this DC motor.

The block diagram of this motor model that will be used here is this one:

![block-diagram.png](block-diagram.png)
Image from: [Methodology to evaluate the reliability of performance of second-order automatic control system - Scientific Figure on ResearchGate.](https://www.researchgate.net/figure/Transfer-function-block-diagram-of-DC-motor_fig3_316198377)

We will start modeling the decoupled system, considering just the motor itself,
without the robot's influence. Then we will consider the robot influence and finally
we will estimate the covariance matrix of this model. Thus, the modeling will
be done step by step, in this way:

- [Decoupled system.](#decoupled-system)
- [Parametrizing the decoupled system.](#parametrizing-the-decoupled-system)
- [Adding robot inertia.](#adding-robot-inertia)
- [Estimating robot inertia.](#estimating-robot-inertia)
- [Making the model stochastic.](#making-the-model-stochastic)

## Decoupled system

The decoupled system is the most basic model we can make, which is basically the block
diagram previously shown. However, some assumptions and modifications are needed.
The inductance parameter (L), will not be considered, this is commonly made as L
is too small compared to the inertial parameter (J) "making J a dominant pole".
Also, the motor has a reduction, this reduction could be ignored leading to different constants when parametrizing the model. But we preferred to treat that
separately. Finally, we assume that no external forces are acting at the motor.

Thus, our motor model block diagram looks like it now:
![decoupled-block-diagram.png](decoupled-block-diagram.png)

All set, we just need to parametrize it now.

## Parametrizing the decoupled system

Using the motor datasheet or factory specification we can calculate all the 
parameters except the inertial parameter (J). Those factory specs were shown at
[motor description](../construction/required_parts.md#motor-with-encoder) and
the useful ones is shown below again:

- Reduction Ratio: 1:100
- No-load current (\(I_{min}\)): 60mA (verified)
- No-load speed (\(\omega_{max}\)): 220rpm (verified)
- Stall current (\(I_{max}\)): 450mA
- Stall torque (\(T_{max}\)): 1.5kg*cm 

Before using those values at any calculation, be sure to convert the units to SI.
We can see that the ratio comes very straight forward, being \(\frac{1}{100}\) . The
stall specs is easy to use as well since the velocity is 0 eliminating the 
system feedback. Thus, 

$$K_a = \frac{T_{max}}{I_{max}} = \frac{0.00147}{0.45} = 0.00327 \space (Nm/A) $$

$$ R = \frac{V}{I_{max}} = \frac{12}{0.45} = 26.667 \space (\Omega) $$

Then, we should use the no-load specs using the [Final Value Theorem](https://en.wikipedia.org/wiki/Final_value_theorem)
since they happens when \( t \rightarrow \infty \). Lets consider the input as a
step to 12V, thus we have these equations to motor speed and current:

$$ lim_{s \rightarrow 0} s \Omega (s) = 
lim_{s \rightarrow 0} s \frac{K_a}{R(Js + B) + K_aK_b} \frac{12}{s} = 
12 \frac{K_a}{R B + K_aK_b} = 2304 $$

$$ lim_{s \rightarrow 0} s I(s) = 
lim_{s \rightarrow 0} s \frac{B}{R(Js + B) + K_aK_b} = 
12 \frac{B}{R B + K_aK_b} = 0.06 $$

Finally, to find the \(K_b\) and \(B\) just solve this system of equations:

\[
\left\{
\begin{array}{ r @{{}={}} r  >{{}}c<{{}} r  >{{}}c<{{}}  r }
12  \frac{K_a}{R B + K_aK_b} & = & 2304 \\
12  \frac{B}{R B + K_aK_b} & = & 0.06 \\
\end{array}
\right.
\]

The solution:
\[
\left\{
\begin{array}{ r @{{}={}} r  >{{}}c<{{}} r  >{{}}c<{{}}  r }
B & = & 8.5*10^{-8} \\
K_b & = & 0.0045 \\
\end{array}
\right.
\]

The last parameter remaining is the inertial parameter (J) that does not affect
the final value but affects the transient response. To find the J value we simply
got the step response for 10.3V input and fit the best J value. The responses are
at the image below:

![motor-response.png](motor-response.png)

FAZER UM GRÁFICO MELHOR E COLOCAR A RESPOSTA DO AJUSTADO.

The best suitable value to it was \(J = 3*10^{-8}\).


