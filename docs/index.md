# Brainiac

The Brainiac is a cheap robot project designed to run Monte Carlo Localization algorithms and to do analyses of its localization performance. This project was created by [Henrique Ferreira Jr.](gitlab.com/henriquejsfj) and [Vinicius Ribeiro](https://gitlab.com/ViniciusRibeiroSouza) as their Degree Final Project in Mechatronics Engineering. 

![Robot image](robot_image.jpeg){: style="display:flex; margin: auto; width:70%"}

The image above shows the Brainiac itself. There is documentation about its required parts, assembly instructions, and more that will be reported later on this page.

## Overview

There are several [repositories](gitlab.com/monte-carlo-robots/brainiac) for this robot in its Gitlab group:

- [Brainiac](https://gitlab.com/monte-carlo-robots/brainiac/brainiac): The ROS package that will run embedded on the Brainiac with all the necessary packages to navigate it.
- [Simulator](https://gitlab.com/monte-carlo-robots/brainiac/brainiac_simulator): The ROS package to simulate the Brainiac.
- [Evaluation System](https://gitlab.com/monte-carlo-robots/brainiac/brainiac_evaluation_system): The ROS package to monitor the Brainiac and evaluate it's Monte Carlo Localization performance.
- [Arduino](https://gitlab.com/monte-carlo-robots/brainiac/arduino): Arduino sketchbook (codes) to set up, test, and run the Brainiac.
- [Manual](https://gitlab.com/monte-carlo-robots/brainiac/manual): Contains this documentation of the Brainiac, feel free to open an issue or improve it.

Note, each ROS package follows the [ROS Package Naming](https://www.ros.org/reps/rep-0144.html) conventions. So the Brainiac is `brainiac` package, Simulator is `brainiac_simulator` and Evaluation System is `brainiac_evaluation_system`.

## Documentation Roadmap

- [Home](./.): This page.
- [Construction](./construction/index.md): Brainiac construction instructions.
    - [Required Parts](./construction/required_parts.md)
    - [Assembly](./construction/assembly.md)
    - [Testing](./construction/testing.md)
- [Usage](./usage/index.md): Brainiac software installation and running instructions.
    - [Robot](./usage/robot.md)
    - [Simulator](./usage/simulator.md)
    - [Evaluation System](./usage/evaluation_system.md)
- [Packages](./packages/index.md): Brainiac ROS packages documentation.
    - [Robot](./packages/robot.md)
    - [Simulator](./packages/simulator.md)
    - [Evaluation System](./packages/evaluation_system.md)
- [Modeling](./modeling/index.md): Brainiac mathematical modeling.
    - [Motor](./modeling/motor.md)
    - [Motor Encoder](./modeling/motor_encoder.md)
    - [IR Sensor](./modeling/ir_sensor.md)
    - [IMU](./modeling/imu.md)
    - [Movement](./modeling/movement.md)
